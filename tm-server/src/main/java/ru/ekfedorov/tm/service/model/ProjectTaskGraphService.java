package ru.ekfedorov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ekfedorov.tm.repository.model.IProjectGraphRepository;
import ru.ekfedorov.tm.repository.model.ITaskGraphRepository;
import ru.ekfedorov.tm.api.service.model.IProjectTaskGraphService;
import ru.ekfedorov.tm.exception.empty.ProjectIdIsEmptyException;
import ru.ekfedorov.tm.exception.empty.TaskIdIsEmptyException;
import ru.ekfedorov.tm.exception.empty.UserIdIsEmptyException;
import ru.ekfedorov.tm.model.TaskGraph;

import java.util.List;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

@Service
public final class ProjectTaskGraphService implements IProjectTaskGraphService {

    @NotNull
    @Autowired
    public ITaskGraphRepository taskGraphRepository;

    @NotNull
    @Autowired
    public IProjectGraphRepository projectGraphRepository;

    @SneakyThrows
    @Override
    public void bindTaskByProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(projectId)) throw new ProjectIdIsEmptyException();
        if (isEmpty(taskId)) throw new TaskIdIsEmptyException();
        taskGraphRepository.bindTaskByProjectId(userId, projectId, taskId);
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<TaskGraph> findAllByProjectId(
            @Nullable final String userId, @Nullable final String projectId
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(projectId)) throw new ProjectIdIsEmptyException();
        return taskGraphRepository.findAllByUserIdAndProjectId(userId, projectId);
    }

    @SneakyThrows
    @Override
    public void removeProjectById(
            @Nullable final String userId, @Nullable final String projectId
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(projectId)) throw new ProjectIdIsEmptyException();
        taskGraphRepository.removeAllByUserIdAndProjectId(userId, projectId);
        projectGraphRepository.removeOneByUserIdAndId(userId, projectId);
    }

    @SneakyThrows
    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId, @Nullable final String taskId
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(taskId)) throw new TaskIdIsEmptyException();
        taskGraphRepository.unbindTaskFromProjectId(userId, taskId);
    }

}
