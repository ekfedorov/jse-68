package ru.ekfedorov.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.api.endpoint.*;
import ru.ekfedorov.tm.api.service.*;
import ru.ekfedorov.tm.api.service.model.*;
import ru.ekfedorov.tm.endpoint.*;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.service.*;
import ru.ekfedorov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Getter
@Setter
@Component
@NoArgsConstructor
public class Bootstrap {

    @NotNull
    @Autowired
    AbstractEndpoint[] abstractEndpoints;

    @NotNull
    public ILoggerService loggerService  = new LoggerService();;

    @NotNull
    @Autowired
    public IPropertyService propertyService;

    @NotNull
    @Autowired
    public IUserGraphService userService;

    @NotNull
    public IActiveMQConnectionService activeMQConnectionService;

    @SneakyThrows
    public void init() {
        initPID();
        initJMSBroker();
        initActiveMQConnectionService();
        initEndpoint();
        initUser();
    }

    private void initActiveMQConnectionService() {
        activeMQConnectionService = new ActiveMQConnectionService();
    }

    @SneakyThrows
    public void initJMSBroker() {
        @NotNull final BrokerService broker = new BrokerService();
        @NotNull final String bindAddress = "tcp://" + BrokerService.DEFAULT_BROKER_NAME + ":" + BrokerService.DEFAULT_PORT;
        broker.addConnector(bindAddress);
        broker.start();
    }

    private void initEndpoint() {
        Arrays.stream(abstractEndpoints).forEach(this::registry);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initUser() {
        if (!userService.findByLogin("test").isPresent()) {
            userService.create("test", "test", "test@test.ru");
        }
        if (!userService.findByLogin("test2").isPresent()) {
            userService.create("test2", "test", "test@test.ru");
        }
        if (!userService.findByLogin("admin").isPresent()) {
            userService.create("admin", "admin", Role.ADMIN);
        }
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

}
