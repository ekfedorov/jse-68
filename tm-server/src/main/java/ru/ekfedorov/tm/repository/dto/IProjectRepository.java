package ru.ekfedorov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.repository.IRepository;
import ru.ekfedorov.tm.dto.Project;

import java.util.List;
import java.util.Optional;

public interface IProjectRepository extends IRepository<Project> {

    void removeByUserId(@NotNull String userId);

    @NotNull
    List<Project> findAllByUserId(@Nullable String userId);

    @NotNull
    Optional<Project> findOneByUserIdAndId(
            @Nullable String userId, @NotNull String id
    );

    Optional<Project> findOneByUserIdAndName(
            @Nullable String userId, @NotNull String name
    );

    void removeOneByUserIdAndId(@Nullable String userId, @NotNull String id);

    void removeOneByUserIdAndName(
            @Nullable String userId, @NotNull String name
    );

}
