package ru.ekfedorov.tm.repository.dto;

import ru.ekfedorov.tm.api.repository.IRepository;
import ru.ekfedorov.tm.dto.Session;

public interface ISessionRepository extends IRepository<Session> {
}
