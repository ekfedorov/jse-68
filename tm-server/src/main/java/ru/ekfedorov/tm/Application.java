package ru.ekfedorov.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.ekfedorov.tm.bootstrap.Bootstrap;
import ru.ekfedorov.tm.cofiguration.ServerConfiguration;

public class Application {

    public static void main(final String[] args) {
        @NotNull AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(ServerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.init();
    }

}
