package ru.ekfedorov.tm.endpontTest;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.ekfedorov.tm.client.ProjectRestEndpointClient;
import ru.ekfedorov.tm.marker.WebCategory;
import ru.ekfedorov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectEndpointTest {

    final Project project = new Project("test", "new");

    final Project project2 = new Project("test2", "new");

    final ProjectRestEndpointClient endpoint = ProjectRestEndpointClient.client();

    @BeforeClass
    public static void beforeClass() {
        ProjectRestEndpointClient.client().deleteAll();
    }

    @Before
    public void before() {
        endpoint.create(project);
    }

    @After
    public void after() {
        endpoint.deleteAll();
    }

    @Test
    @Category(WebCategory.class)
    public void find() {
        Assert.assertEquals(project.getName(), endpoint.find(project.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void create() {
        Assert.assertNotNull(endpoint.find(project.getId()));
    }

    @Test
    @Category(WebCategory.class)
    public void update() {
        final Project updatedProject = endpoint.find(project.getId());
        updatedProject.setName("updated");
        endpoint.save(updatedProject);
        Assert.assertEquals("updated", endpoint.find(project.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void delete() {
        endpoint.delete(project.getId());
        Assert.assertNull(endpoint.find(project.getId()));
    }

    @Test
    @Category(WebCategory.class)
    public void findAll() {
        Assert.assertEquals(1, endpoint.findAll().size());
        endpoint.create(project2);
        Assert.assertEquals(2, endpoint.findAll().size());
    }

    @Test
    @Category(WebCategory.class)
    public void updateAll() {
        endpoint.create(project2);
        final Project updatedProject = endpoint.find(project.getId());
        updatedProject.setName("updated1");
        final Project updatedProject2 = endpoint.find(project2.getId());
        updatedProject2.setName("updated2");
        List<Project> updatedList = new ArrayList<>();
        updatedList.add(updatedProject);
        updatedList.add(updatedProject2);
        endpoint.saveAll(updatedList);
        Assert.assertEquals("updated1", endpoint.find(project.getId()).getName());
        Assert.assertEquals("updated2", endpoint.find(project2.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void createAll() {
        endpoint.delete(project.getId());
        List<Project> list = new ArrayList<>();
        list.add(project);
        list.add(project2);
        endpoint.createAll(list);
        Assert.assertEquals("test", endpoint.find(project.getId()).getName());
        Assert.assertEquals("test2", endpoint.find(project2.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void deleteAll() {
        endpoint.create(project2);
        Assert.assertEquals(2, endpoint.findAll().size());
        endpoint.deleteAll();
        Assert.assertEquals(0, endpoint.findAll().size());
    }


}
