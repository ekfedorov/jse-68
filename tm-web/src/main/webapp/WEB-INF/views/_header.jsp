<html>
<style>@import url("/styles/main.css");</style>
<head>
  <title>${view_name}</title>
</head>
<body>
<table style="width: 100%">
  <tr>
    <td class="head">
      <div>TASK MANAGER</div>
    </td>
    <td class="mini">
      <a href="/tasks">TASKS</a>
    </td>
    <td class="mini">
      <a href="/projects">PROJECTS</a>
    </td>
  </tr>
</table>
<table class="upper-head">
  <tr>
    <td>
      <div>${view_name}</div>
    </td>
  </tr>
</table>
