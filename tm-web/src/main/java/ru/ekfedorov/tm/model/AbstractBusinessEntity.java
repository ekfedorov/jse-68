package ru.ekfedorov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.PropertyKey;
import org.springframework.format.annotation.DateTimeFormat;
import ru.ekfedorov.tm.enumerated.Status;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractBusinessEntity {

    @Id
    protected String id = UUID.randomUUID().toString();

    protected Date created = new Date();

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date dateFinish;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date dateStart;

    protected String description = "";

    protected String name = "";

    protected Status status = Status.NOT_STARTED;

}

