package ru.ekfedorov.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ekfedorov.tm.api.service.ITaskService;
import ru.ekfedorov.tm.exception.EmptyIdException;
import ru.ekfedorov.tm.model.Task;
import ru.ekfedorov.tm.repository.ITaskRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class TaskService implements ITaskService {

    @Autowired
    private ITaskRepository repository;

    @Override
    public List<Task> findAll() {
        return repository.findAll();
    }

    @Override
    public void addAll(final Collection<Task> collection) {
        if (collection == null) return;
        for (Task item : collection) {
            add(item);
        }
    }

    @Override
    public Task add(final Task entity) {
        if (entity == null) return null;
        repository.save(entity);
        return entity;
    }

    @Override
    public void create() {
        repository.save(new Task("task", "new"));
    }

    @Override
    public Task findById(final String id) {
        final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findById(optionalId.orElseThrow(EmptyIdException::new)).get();
    }

    @Override
    public void clear() {
        repository.deleteAll();
    }

    @Override
    public void removeById(final String id) {
        final Optional<String> optionalId = Optional.ofNullable(id);
        repository.deleteById(optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    public void remove(final Task entity) {
        if (entity == null) return;
        repository.deleteById(entity.getId());
    }

}
