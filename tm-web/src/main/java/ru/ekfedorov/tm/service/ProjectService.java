package ru.ekfedorov.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ekfedorov.tm.api.service.IProjectService;
import ru.ekfedorov.tm.exception.EmptyIdException;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.repository.IProjectRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class ProjectService implements IProjectService {

    @Autowired
    private IProjectRepository repository;

    @Override
    public List<Project> findAll() {
        return repository.findAll();
    }

    @Override
    public void addAll(final Collection<Project> collection) {
        if (collection == null) return;
        for (Project item : collection) {
            add(item);
        }
    }

    @Override
    public Project add(final Project entity) {
        if (entity == null) return null;
        repository.save(entity);
        return entity;
    }

    @Override
    public void create() {
        repository.save(new Project("project", "new"));
    }

    @Override
    public Project findById(final String id) {
        final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findById(optionalId.orElseThrow(EmptyIdException::new)).get();
    }

    @Override
    public void clear() {
        repository.deleteAll();
    }

    @Override
    public void removeById(final String id) {
        final Optional<String> optionalId = Optional.ofNullable(id);
        repository.deleteById(optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    public void remove(final Project entity) {
        if (entity == null) return;
        repository.deleteById(entity.getId());
    }

}

