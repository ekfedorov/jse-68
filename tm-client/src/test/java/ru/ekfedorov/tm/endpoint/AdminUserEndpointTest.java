package ru.ekfedorov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.ekfedorov.tm.api.endpoint.EndpointLocator;
import ru.ekfedorov.tm.bootstrap.Bootstrap;
import ru.ekfedorov.tm.cofiguration.ClientConfiguration;
import ru.ekfedorov.tm.marker.IntegrationCategory;

public class AdminUserEndpointTest {

    @NotNull
    public final AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ClientConfiguration.class);

    @NotNull
    private final SessionEndpoint sessionEndpoint = context.getBean(SessionEndpoint.class);

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = context.getBean(AdminUserEndpoint.class);

    @NotNull
    private final UserEndpoint userEndpoint = context.getBean(UserEndpoint.class);

    private Session sessionAdmin;

    @Before
    @SneakyThrows
    public void before() {
        sessionAdmin = sessionEndpoint.openSession("admin", "admin");
    }

    @After
    @SneakyThrows
    public void after() {
        sessionEndpoint.closeSession(sessionAdmin);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void createUserWithRole() {
        adminUserEndpoint.createUserWithRole(sessionAdmin, "testCreateWR", "test2", Role.ADMIN);
        final User user = userEndpoint.findUserByLogin(sessionAdmin, "testCreateWR");
        Assert.assertEquals(Role.ADMIN, user.getRole());
        adminUserEndpoint.removeByLogin(sessionAdmin, "testCreateWR");
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findAllUser() {
        Assert.assertFalse(adminUserEndpoint.findAllUser(sessionAdmin).isEmpty());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findUserOneById() {
        Assert.assertNotNull(adminUserEndpoint.findUserOneById(sessionAdmin, sessionAdmin.getUserId()));
    }

//    @Test(expected = UserIsLockedException_Exception.class)
//    @SneakyThrows
//    @Category(IntegrationCategory.class)
//    public void lockUserByLogin() {
//        adminUserEndpoint.lockUserByLogin(sessionAdmin, "test");
//        sessionEndpoint.openSession("test", "test");
//    }TODO

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeByLogin() {
        adminUserEndpoint.removeByLogin(sessionAdmin, "testRemoveByLogin");
        userEndpoint.createUser("testRemoveByLogin", "test", "test@twst.ru");
        adminUserEndpoint.removeByLogin(sessionAdmin, "testRemoveByLogin");
        Assert.assertNull(userEndpoint.findUserByLogin(sessionAdmin, "testRemoveByLogin"));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeUserOneById() {
        adminUserEndpoint.removeByLogin(sessionAdmin, "testRemoveById");
        userEndpoint.createUser("testRemoveById", "test", "test@twst.ru");
        final User user = userEndpoint.findUserByLogin(sessionAdmin, "testRemoveById");
        adminUserEndpoint.removeUserOneById(sessionAdmin, user.getId());
        Assert.assertNull(userEndpoint.findUserByLogin(sessionAdmin, "testRemoveById"));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void unlockUserByLogin() {
        adminUserEndpoint.lockUserByLogin(sessionAdmin, "test");
        adminUserEndpoint.unlockUserByLogin(sessionAdmin, "test");
        Assert.assertFalse(userEndpoint.findUserByLogin(sessionAdmin, "test").isLocked());
    }

}
