package ru.ekfedorov.tm.listener.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.event.ConsoleEvent;
import ru.ekfedorov.tm.listener.AbstractProjectListener;
import ru.ekfedorov.tm.endpoint.*;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.exception.system.NullProjectException;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class ProjectChangeStatusByNameListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Change project status by name.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "change-project-status-by-name";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@projectChangeStatusByNameListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new NullObjectException();
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final Project project = projectEndpoint.findProjectOneByName(session, name);
        if (project == null) throw new NullProjectException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusId = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusId);
        projectEndpoint.changeProjectStatusByName(session, name, status);
    }

}
